﻿using Mecanica.DB;
using Mecanica.DB.Cliente;
using Mecanica.DB.Funcionario;
using Mecanica.DB.Serviço;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mecanica
{
    public partial class Tela_serviço : Form
    {
        public Tela_serviço()
        {
            InitializeComponent();

            CarregarCombos();
        }

        private void Tela_serviço_Load(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void CarregarCombos()
        {
            ClienteBusiness business = new ClienteBusiness();
            List<ClientesDTO> lista = business.Listar();

            cboCliente.ValueMember = nameof(ClientesDTO.Id);
            cboCliente.DisplayMember = nameof(ClientesDTO.Nome);
            cboCliente.DataSource = lista;


            FuncionarioBusiness business2 = new FuncionarioBusiness();
            List<FuncionarioDTO> lista2 = business2.Listar();

            cboFuncionario.ValueMember = nameof(FuncionarioDTO.Id);
            cboFuncionario.DisplayMember = nameof(FuncionarioDTO.Nome);
            cboFuncionario.DataSource = lista2;




        }



        private void button1_Click(object sender, EventArgs e)
        {

            
          
                FuncionarioDTO funcionario = cboFuncionario.SelectedItem as FuncionarioDTO;

                ClientesDTO Cliente = cboCliente.SelectedItem as ClientesDTO;





                OrdemServicoDTO dto = new OrdemServicoDTO();
                dto.Id_cliente = Cliente.Id;
                dto.Id_funcionario = funcionario.Id;
                dto.Valor = Convert.ToDecimal(maskedtxtValor.Text);
                dto.Dtservico = DateTime.Now;
                dto.descricao = maskedtxtDescricao.Text;


                OrdemServicoBusiness business = new OrdemServicoBusiness();
                business.Salvar(dto);

                EnviarMensagem("Serviço salvo com sucesso.");


                this.Hide();

                Menu_Principal cadastro = new Menu_Principal();

                cadastro.Show();

     




        }

        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "MARBRU",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Voltar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();

            Menu_Principal cadastro = new Menu_Principal();

            cadastro.Show();
        }

        private void txtDescricao_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
