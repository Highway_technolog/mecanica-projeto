﻿using Mecanica.DB.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mecanica
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {
                FuncionarioBusiness business = new FuncionarioBusiness();
                bool funcionario = business.Logar(txtEmail.Text, txtSenha.Text);

                if (funcionario == true)
                {



                    Menu_Principal frm = new Menu_Principal();
                    frm.Show();
                    this.Close();


                }

            }
            catch (Exception)
            {

                MessageBox.Show("Insira as informações");
            }
            
        }

        private void textSenha_TextChanged(object sender, EventArgs e)
        {

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            CondastroFuncLogin frm = new CondastroFuncLogin();
            frm.Show();
            this.Close();


        }
    }
}
