﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mecanica
{
    public partial class frmSplash : Form
    {
        public frmSplash()
        {
            InitializeComponent();
            timerProgress.Start();

        }

        private void frmSplash_Load(object sender, EventArgs e)
        {

        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void timerProgress_Tick(object sender, EventArgs e)
        {
            progressBar1.Increment(1);
            if (progressBar1.Value == 100)
            {
                timerProgress.Stop();
                Login frm = new Login();
                frm.Show();
                Hide();
            }
        }
    }
}
