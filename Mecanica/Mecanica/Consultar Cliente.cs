﻿using Mecanica.DB;
using Mecanica.DB.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mecanica
{
    public partial class Consultar_Cliente : Form
    {
        public Consultar_Cliente()
        {
            InitializeComponent();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
                CarregarGrid();
            

        }
             public void CarregarGrid()
            {


            string cliente = txtBusca.Text;

            ClienteBusiness business = new ClienteBusiness();
            

            List<ClienteConsultaView> view = business.Consultar(txtBusca.Text.Trim());

                
            gvCliente.AutoGenerateColumns = false;
            gvCliente.DataSource = view;


            //
            // try
            //  {
            //  string cliente = txtBusca.Text;


            //  ClienteBusiness business = new ClienteBusiness();
            //      List<ClientesDTO> clientes = business.Consultar(cliente);

            //            gvCliente.AutoGenerateColumns = false;
            //     gvCliente.DataSource = clientes;
            //   }
            //    catch (Exception ex)
            //  {
            //   MessageBox.Show("Ocorreu um erro ao salvar a música: " + ex.Message, "Nsf Jam",
            //  MessageBoxButtons.OK,
            //  MessageBoxIcon.Error);
            //  }
        }



        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                ClientesDTO musica = gvCliente.CurrentRow.DataBoundItem as ClientesDTO;

                DialogResult r = MessageBox.Show("Deseja excluir o cliente?", "Nsf Jam",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    ClienteBusiness business = new ClienteBusiness();
                    business.Remover(musica.Id);

                    CarregarGrid();
                }
            }
        }

        private void Voltar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();

            Menu_Principal cadastro = new Menu_Principal();

            cadastro.Show();
        }
    }
}
