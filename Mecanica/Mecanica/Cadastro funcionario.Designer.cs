﻿namespace Mecanica
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.dtpNascimento = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.cboFuncao = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.Voltar = new System.Windows.Forms.LinkLabel();
            this.maskedtxtNome = new System.Windows.Forms.MaskedTextBox();
            this.maskedtxtRg = new System.Windows.Forms.MaskedTextBox();
            this.maskedtxtSenha = new System.Windows.Forms.MaskedTextBox();
            this.maskedtxtCPF = new System.Windows.Forms.MaskedTextBox();
            this.maskedtxtSalario = new System.Windows.Forms.MaskedTextBox();
            this.maskedtxtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.maskedtxtEmail = new System.Windows.Forms.MaskedTextBox();
            this.maskedtxtCelular = new System.Windows.Forms.MaskedTextBox();
            this.maskedtxtEndereco = new System.Windows.Forms.MaskedTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Gold;
            this.pictureBox1.Location = new System.Drawing.Point(614, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(20, 424);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Gold;
            this.pictureBox2.Location = new System.Drawing.Point(-1, -1);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(20, 424);
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Gold;
            this.pictureBox3.Location = new System.Drawing.Point(-12, -1);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(646, 20);
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Gold;
            this.pictureBox4.Location = new System.Drawing.Point(-12, 403);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(646, 20);
            this.pictureBox4.TabIndex = 4;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Segoe Print", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Gold;
            this.label1.Location = new System.Drawing.Point(131, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(411, 56);
            this.label1.TabIndex = 5;
            this.label1.Text = "Cadastro de funcionário";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Gold;
            this.label2.Location = new System.Drawing.Point(62, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 26);
            this.label2.TabIndex = 6;
            this.label2.Text = "Nome :";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Gold;
            this.label3.Location = new System.Drawing.Point(82, 182);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 26);
            this.label3.TabIndex = 8;
            this.label3.Text = "RG :";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Gold;
            this.label4.Location = new System.Drawing.Point(56, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 26);
            this.label4.TabIndex = 9;
            this.label4.Text = "Senha :";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Gold;
            this.label5.Location = new System.Drawing.Point(337, 95);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 26);
            this.label5.TabIndex = 10;
            this.label5.Text = "E-mail :";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Gold;
            this.label6.Location = new System.Drawing.Point(72, 229);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 26);
            this.label6.TabIndex = 11;
            this.label6.Text = "CPF :";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Gold;
            this.label7.Location = new System.Drawing.Point(35, 308);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 26);
            this.label7.TabIndex = 16;
            this.label7.Text = "Endereço :";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Gold;
            this.label8.Location = new System.Drawing.Point(343, 184);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 26);
            this.label8.TabIndex = 18;
            this.label8.Text = "Celular:";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Gold;
            this.label9.Location = new System.Drawing.Point(346, 230);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 26);
            this.label9.TabIndex = 21;
            this.label9.Text = "Salário: ";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Black;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.Gold;
            this.button2.Location = new System.Drawing.Point(467, 353);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 23;
            this.button2.Text = "Cadastrar";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Black;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.ForeColor = System.Drawing.Color.Gold;
            this.button3.Location = new System.Drawing.Point(179, 353);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 24;
            this.button3.Text = "Cancelar";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Gold;
            this.label10.Location = new System.Drawing.Point(21, 274);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(109, 23);
            this.label10.TabIndex = 25;
            this.label10.Text = "Data de Nasc: ";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // dtpNascimento
            // 
            this.dtpNascimento.Location = new System.Drawing.Point(130, 274);
            this.dtpNascimento.MaxDate = new System.DateTime(2000, 12, 31, 0, 0, 0, 0);
            this.dtpNascimento.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dtpNascimento.Name = "dtpNascimento";
            this.dtpNascimento.Size = new System.Drawing.Size(180, 20);
            this.dtpNascimento.TabIndex = 26;
            this.dtpNascimento.Value = new System.DateTime(2000, 12, 31, 0, 0, 0, 0);
            this.dtpNascimento.ValueChanged += new System.EventHandler(this.dtpNascimento_ValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Gold;
            this.label11.Location = new System.Drawing.Point(346, 271);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(75, 26);
            this.label11.TabIndex = 27;
            this.label11.Text = "Função: ";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // cboFuncao
            // 
            this.cboFuncao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFuncao.FormattingEnabled = true;
            this.cboFuncao.Location = new System.Drawing.Point(417, 277);
            this.cboFuncao.Name = "cboFuncao";
            this.cboFuncao.Size = new System.Drawing.Size(191, 21);
            this.cboFuncao.TabIndex = 28;
            this.cboFuncao.SelectedIndexChanged += new System.EventHandler(this.cboFuncao_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Gold;
            this.label12.Location = new System.Drawing.Point(337, 140);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 26);
            this.label12.TabIndex = 29;
            this.label12.Text = "Telefone: ";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Gold;
            this.label13.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(610, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 19);
            this.label13.TabIndex = 31;
            this.label13.Text = "X";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // Voltar
            // 
            this.Voltar.ActiveLinkColor = System.Drawing.Color.Yellow;
            this.Voltar.AutoSize = true;
            this.Voltar.BackColor = System.Drawing.SystemColors.Desktop;
            this.Voltar.DisabledLinkColor = System.Drawing.Color.Olive;
            this.Voltar.LinkColor = System.Drawing.Color.Yellow;
            this.Voltar.Location = new System.Drawing.Point(22, 22);
            this.Voltar.Name = "Voltar";
            this.Voltar.Size = new System.Drawing.Size(34, 13);
            this.Voltar.TabIndex = 32;
            this.Voltar.TabStop = true;
            this.Voltar.Text = "Voltar";
            this.Voltar.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Voltar_LinkClicked);
            // 
            // maskedtxtNome
            // 
            this.maskedtxtNome.Location = new System.Drawing.Point(130, 94);
            this.maskedtxtNome.Name = "maskedtxtNome";
            this.maskedtxtNome.Size = new System.Drawing.Size(180, 20);
            this.maskedtxtNome.TabIndex = 33;
            this.maskedtxtNome.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedTextBox1_MaskInputRejected);
            this.maskedtxtNome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.maskedtxtNome_KeyPress);
            // 
            // maskedtxtRg
            // 
            this.maskedtxtRg.Location = new System.Drawing.Point(130, 190);
            this.maskedtxtRg.Mask = "99.999.999-9";
            this.maskedtxtRg.Name = "maskedtxtRg";
            this.maskedtxtRg.Size = new System.Drawing.Size(180, 20);
            this.maskedtxtRg.TabIndex = 34;
            this.maskedtxtRg.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedtxtRg_MaskInputRejected);
            // 
            // maskedtxtSenha
            // 
            this.maskedtxtSenha.Location = new System.Drawing.Point(130, 139);
            this.maskedtxtSenha.Name = "maskedtxtSenha";
            this.maskedtxtSenha.PasswordChar = '°';
            this.maskedtxtSenha.Size = new System.Drawing.Size(180, 20);
            this.maskedtxtSenha.TabIndex = 35;
            this.maskedtxtSenha.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedtxtSenha_MaskInputRejected);
            // 
            // maskedtxtCPF
            // 
            this.maskedtxtCPF.Location = new System.Drawing.Point(130, 236);
            this.maskedtxtCPF.Mask = "999999999-99";
            this.maskedtxtCPF.Name = "maskedtxtCPF";
            this.maskedtxtCPF.Size = new System.Drawing.Size(180, 20);
            this.maskedtxtCPF.TabIndex = 36;
            this.maskedtxtCPF.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedtxtCPF_MaskInputRejected);
            // 
            // maskedtxtSalario
            // 
            this.maskedtxtSalario.Location = new System.Drawing.Point(417, 236);
            this.maskedtxtSalario.Name = "maskedtxtSalario";
            this.maskedtxtSalario.Size = new System.Drawing.Size(191, 20);
            this.maskedtxtSalario.TabIndex = 37;
            this.maskedtxtSalario.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedtxtSalario_MaskInputRejected);
            // 
            // maskedtxtTelefone
            // 
            this.maskedtxtTelefone.Location = new System.Drawing.Point(417, 146);
            this.maskedtxtTelefone.Mask = "(99) 9999-9999";
            this.maskedtxtTelefone.Name = "maskedtxtTelefone";
            this.maskedtxtTelefone.Size = new System.Drawing.Size(191, 20);
            this.maskedtxtTelefone.TabIndex = 38;
            this.maskedtxtTelefone.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedtxtTelefone_MaskInputRejected);
            // 
            // maskedtxtEmail
            // 
            this.maskedtxtEmail.Location = new System.Drawing.Point(417, 100);
            this.maskedtxtEmail.Name = "maskedtxtEmail";
            this.maskedtxtEmail.Size = new System.Drawing.Size(191, 20);
            this.maskedtxtEmail.TabIndex = 39;
            this.maskedtxtEmail.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedtxtEmail_MaskInputRejected);
            // 
            // maskedtxtCelular
            // 
            this.maskedtxtCelular.Location = new System.Drawing.Point(417, 188);
            this.maskedtxtCelular.Mask = "(99) 99999-9999";
            this.maskedtxtCelular.Name = "maskedtxtCelular";
            this.maskedtxtCelular.Size = new System.Drawing.Size(191, 20);
            this.maskedtxtCelular.TabIndex = 40;
            this.maskedtxtCelular.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedtxtCelular_MaskInputRejected);
            // 
            // maskedtxtEndereco
            // 
            this.maskedtxtEndereco.Location = new System.Drawing.Point(130, 308);
            this.maskedtxtEndereco.Name = "maskedtxtEndereco";
            this.maskedtxtEndereco.Size = new System.Drawing.Size(478, 20);
            this.maskedtxtEndereco.TabIndex = 41;
            this.maskedtxtEndereco.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedtxtEndereco_MaskInputRejected);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(631, 422);
            this.Controls.Add(this.maskedtxtEndereco);
            this.Controls.Add(this.maskedtxtCelular);
            this.Controls.Add(this.maskedtxtEmail);
            this.Controls.Add(this.maskedtxtTelefone);
            this.Controls.Add(this.maskedtxtSalario);
            this.Controls.Add(this.maskedtxtCPF);
            this.Controls.Add(this.maskedtxtSenha);
            this.Controls.Add(this.maskedtxtRg);
            this.Controls.Add(this.maskedtxtNome);
            this.Controls.Add(this.Voltar);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.cboFuncao);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.dtpNascimento);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker dtpNascimento;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cboFuncao;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.LinkLabel Voltar;
        private System.Windows.Forms.MaskedTextBox maskedtxtNome;
        private System.Windows.Forms.MaskedTextBox maskedtxtRg;
        private System.Windows.Forms.MaskedTextBox maskedtxtSenha;
        private System.Windows.Forms.MaskedTextBox maskedtxtCPF;
        private System.Windows.Forms.MaskedTextBox maskedtxtSalario;
        private System.Windows.Forms.MaskedTextBox maskedtxtTelefone;
        private System.Windows.Forms.MaskedTextBox maskedtxtEmail;
        private System.Windows.Forms.MaskedTextBox maskedtxtCelular;
        private System.Windows.Forms.MaskedTextBox maskedtxtEndereco;
    }
}