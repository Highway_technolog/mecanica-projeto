﻿using Mecanica.DB.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mecanica
{
    public partial class CondastroFuncLogin : Form
    {
        public CondastroFuncLogin()
        {
            InitializeComponent();
            CarregarFuncao();
        }
        private void CarregarFuncao()
        {
            List<string> Funcao = new List<string>();
            Funcao.Add("Selecione");
            Funcao.Add("Mecânico");
            Funcao.Add("Limpeza");
            Funcao.Add("Administração");
            Funcao.Add("Caixa");


            cboFuncao.DataSource = Funcao;
        }
        private void CondastroFuncLogin_Load(object sender, EventArgs e)
        {

        }
        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "MARBRU",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO funcionario = new FuncionarioDTO();
                funcionario.Nome = maskedtxtNome.Text;
                funcionario.Senha = maskedtxtSenha.Text;
                funcionario.RG = maskedtxtRg.Text;
                funcionario.CPF = maskedtxtCPF.Text;
                funcionario.Nascimento = dtpNascimento.Value;
                funcionario.Email = maskedtxtEmail.Text;
                funcionario.Telefone = maskedtxtTelefone.Text;
                funcionario.Endereco = maskedtxtEndereco.Text;
                funcionario.Celular = maskedtxtCelular.Text;
                funcionario.Funcao = cboFuncao.Text;
                funcionario.Salario = Convert.ToDouble(maskedtxtSalario.Text);



                FuncionarioBusiness business = new FuncionarioBusiness();
                business.Salvar(funcionario);

                EnviarMensagem("Funcionário salvo com sucesso.");

                this.Hide();

                Menu_Principal cadastro = new Menu_Principal();

                cadastro.Show();

            }
            catch (Exception)
            {

                MessageBox.Show("Use apenas informações válidas");
            }
        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();

            Login cadastro = new Login();

            cadastro.Show();
        }

        private void maskedtxtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {

                e.Handled = true;

            }
            else
            {
                e.Handled = false;
            }



        }

        private void maskedtxtNome_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }
    }
}
