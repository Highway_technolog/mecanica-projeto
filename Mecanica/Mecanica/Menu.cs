﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mecanica
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();

            Form2 cadastro = new Form2();

            cadastro.Show();
        }

        private void Voltar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();

            Menu_Principal cadastro = new Menu_Principal();

            cadastro.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();

            Form1 cadastro = new Form1();

            cadastro.Show();
        }

        private void Consultas_Click(object sender, EventArgs e)
        {
            this.Hide();

            Cadastro_Carro cadastro = new Cadastro_Carro();

            cadastro.Show();
        }
    }
}
