﻿using Mecanica.DB.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mecanica
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            CarregarFuncao();
        }

        private void CarregarFuncao()
        {
            List<string> Funcao = new List<string>();
            Funcao.Add("Selecione");
            Funcao.Add("Mecânico");
            Funcao.Add("Limpeza");
            Funcao.Add("Administração");
            Funcao.Add("Caixa");
            
            
            cboFuncao.DataSource = Funcao;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Voltar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();

            Menu_Principal cadastro = new Menu_Principal();

            cadastro.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();

            Menu cadastro = new Menu();

            cadastro.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            
                FuncionarioDTO funcionario = new FuncionarioDTO();
                funcionario.Nome = maskedtxtNome.Text;
                funcionario.Senha = maskedtxtSenha.Text;
                funcionario.RG = maskedtxtRg.Text;
                funcionario.CPF = maskedtxtCPF.Text;
                funcionario.Nascimento = dtpNascimento.Value;
                funcionario.Email = maskedtxtEmail.Text;
                funcionario.Telefone = maskedtxtTelefone.Text;
                funcionario.Endereco = maskedtxtEndereco.Text;
                funcionario.Celular = maskedtxtCelular.Text;
                funcionario.Funcao = cboFuncao.Text;
                funcionario.Salario = Convert.ToDouble(maskedtxtSalario.Text);

                string asd =maskedtxtEmail.Text;
                bool email =asd.Contains ("@");

                if (email==false)
                {
                    MessageBox.Show("Esse tipo de Email não é valído");
                }



                FuncionarioBusiness business = new FuncionarioBusiness();
                business.Salvar(funcionario);

                EnviarMensagem("Funcionário salvo com sucesso.");

     
        }      
            
        
            
            

        

        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "MARBRU",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void maskedtxtCelular_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void maskedtxtEmail_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void maskedtxtTelefone_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void maskedtxtSalario_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void maskedtxtCPF_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void maskedtxtSenha_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void maskedtxtRg_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void cboFuncao_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void dtpNascimento_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void maskedtxtEndereco_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void maskedtxtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {

                e.Handled = true;

            }
            else
            {
                e.Handled = false;
            }
        }
    }
}
