﻿using Mecanica.DB.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mecanica
{
    public partial class Consultar_funcionarios : Form
    {
        public Consultar_funcionarios()
        {
            InitializeComponent();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Consultar_funcionarios_Load(object sender, EventArgs e)
        {

        }

       

        private void button1_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        public void CarregarGrid()
        {
            try
            {
                string Nome = txtBusca.Text;
                

                FuncionarioBusiness business = new FuncionarioBusiness();
                List<FuncionarioDTO> musicas = business.Consultar(Nome);

                gvFuncionario.AutoGenerateColumns = false;
                gvFuncionario.DataSource = musicas;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro ao salvar o funcionário: " + ex.Message, "BRAMUR",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                FuncionarioDTO musica = gvFuncionario.CurrentRow.DataBoundItem as FuncionarioDTO;

                DialogResult r = MessageBox.Show("Deseja excluir o funcionário?", "BRAMUR",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    FuncionarioBusiness business = new FuncionarioBusiness();
                    business.Remover(musica.Id);

                    CarregarGrid();
                }
            }
        }

        private void Voltar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();

            Menu_Principal cadastro = new Menu_Principal();

            cadastro.Show();
        }
    }
}
