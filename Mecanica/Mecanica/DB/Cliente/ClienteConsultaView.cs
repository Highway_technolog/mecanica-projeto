﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mecanica.DB.Cliente
{
    class ClienteConsultaView
    {
        public string Nome { get; set; }
        public string Carro { get; set; }
        public string Telefone { get; set; }
        public string Observacao { get; set; }
        public int Id { get; set; }


    }
}
