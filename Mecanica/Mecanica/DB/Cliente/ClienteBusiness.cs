﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mecanica.DB.Cliente
{
    public class ClienteBusiness
    {
       
            ClienteDatabase db = new ClienteDatabase();

            public int Salvar(ClientesDTO cliente)
            {
                if (cliente.Nome == string.Empty)
                {
                    throw new ArgumentException("Nome é obrigatória.");
                }
                if (cliente.CPF == null)
                {
                    throw new ArgumentException("CPf é obrigatória.");
                }
                 if (cliente.Nascimento == null)
                {
                    throw new ArgumentException("Nascimento é obrigatória.");
                }

                return db.Salvar(cliente);
            }

            public void Alterar(ClientesDTO musica)
            {
                if (musica.Nome == string.Empty)
                {
                    throw new ArgumentException("música é obrigatória.");
                }
                if (musica.CPF == null)
                {
                    throw new ArgumentException("duração é obrigatória.");
                }

                db.Alterar(musica);
            }

            public void Remover(int id)
            {
                db.Remover(id);
            }

        public List<ClienteConsultaView> Consultar(string cliente)
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.Consultar(cliente);
        }

        public List<ClientesDTO> Listar()
            {
           ClienteDatabase db = new ClienteDatabase();
            return db.Listar();
            }



    }
}
