﻿using Mecanica.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mecanica.DB.Cliente
{
    public class MarcasDatabase
    {
        public int Salvar(MarcasDTO marca)
        {
            string script =
            @"INSERT INTO tb_marcas
            (
	            nm_marcas
	           
            )
            VALUES
            (
	            @nm_marcas
            )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_marcas", marca.Marca));


            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(MarcasDTO marca)
        {
            string script =
            @"UPDATE tb_marcas 
                 SET nm_marcas       = @nm_marcas
               WHERE id_marcas       = @id_marcas";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_marcas", marca.Id));
            parms.Add(new MySqlParameter("nm_marcas", marca.Marca));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(int id)
        {
            string script =
            @"DELETE FROM tb_marcas WHERE id_marcas = @id_marcas";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_marcas", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<MarcasDTO> Consultar(string marca)
        {
            string script =
            @"SELECT * 
                FROM tb_marcas
               WHERE nm_marcas like @nm_marcas";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_modelo", "%" + marca + "%"));


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<MarcasDTO> Marcas = new List<MarcasDTO>();

            while (reader.Read())
            {
                MarcasDTO marcas = new MarcasDTO();
                marcas.Id = reader.GetInt32("id_marca");
                marcas.Marca = reader.GetString("nm_marcas");



                Marcas.Add(marcas);
            }
            reader.Close();

            return Marcas;
        }

        public List<MarcasDTO> Listar()
        {
            string script = @"SELECT * FROM tb_carro";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<MarcasDTO> lista = new List<MarcasDTO>();
            while (reader.Read())
            {
                MarcasDTO dto = new MarcasDTO();
                dto.Id = reader.GetInt32("id_marca");
                dto.Marca = reader.GetString("nm_marcas");

                lista.Add(dto);
            }
            reader.Close();

            return lista;




        }
    }  }
