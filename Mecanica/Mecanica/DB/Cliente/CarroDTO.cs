﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mecanica.DB.Cliente
{
    public class CarroDTO
    {
        public int Id { get; set; }
        public int Id_Cliente { get; set; }
        public string Modelo { get; set; }
        public DateTime Ano{ get; set; }
        public string Placa { get; set; }
        public string Observacao { get; set; }
        public string Marca { get; set; }


    }
}
