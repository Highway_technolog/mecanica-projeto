﻿using Mecanica.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mecanica.DB.Cliente
{
    public class ClienteDatabase
    {
        public int Salvar(ClientesDTO cliente)
        {
            string script =
            @"INSERT INTO tb_cliente
            (
	            nm_cliente,
	            ds_rg,
	            ds_cpf,
	            dt_nascimento,
	            ds_email,
	            ds_telefone,
	            ds_endereco,
                ds_observacao
            )
            VALUES
            (
	            @nm_cliente,
	            @ds_rg,
	            @ds_cpf,
	            @dt_nascimento,
	            @ds_email,
	            @ds_telefone,
	            @ds_endereco,
                @ds_observacao
            )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", cliente.Nome));
            parms.Add(new MySqlParameter("ds_rg", cliente.Rg));
            parms.Add(new MySqlParameter("ds_cpf", cliente.CPF));
            parms.Add(new MySqlParameter("dt_nascimento", cliente.Nascimento));
            parms.Add(new MySqlParameter("ds_email", cliente.Email));
            parms.Add(new MySqlParameter("ds_telefone", cliente.Telefone));
            parms.Add(new MySqlParameter("ds_endereco", cliente.Endereco));
            parms.Add(new MySqlParameter("ds_observacao", cliente.Observacao));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(ClientesDTO cliente)
        {
            string script =
            @"UPDATE tb_cliente 
                 SET nm_cliente  = @nm_cliente,
	                 ds_rg       = @ds_rg,
	                 ds_cpf      = @ds_cpf,
	                 dt_nascimento = @dt_nascimento,
	                 ds_email  = @ds_email,
	                 ds_telefone = @ds_telefone,
	                 ds_endereco = @ds_endereco
                     ds_observacao = @ds_observacao
               WHERE id_musica = @id_musica";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", cliente.Id));
            parms.Add(new MySqlParameter("nm_nome", cliente.Nome));
            parms.Add(new MySqlParameter("ds_rg", cliente.Rg));
            parms.Add(new MySqlParameter("ds_cpf", cliente.CPF));
            parms.Add(new MySqlParameter("dt_nascimento", cliente.Nascimento));
            parms.Add(new MySqlParameter("ds_email", cliente.Email));
            parms.Add(new MySqlParameter("ds_telefone", cliente.Telefone));
            parms.Add(new MySqlParameter("ds_endereco", cliente.Endereco));
            parms.Add(new MySqlParameter("ds_observacao", cliente.Observacao));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(int id)
        {
            string script =
            @"DELETE FROM tb_cliente WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ClienteConsultaView> Consultar(string cliente)
        {
            string script = @"SELECT * FROM vw_cliente_consulta WHERE nm_cliente like @nm_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteConsultaView> lista = new List<ClienteConsultaView>();
            while (reader.Read())
            {
                ClienteConsultaView dto = new ClienteConsultaView();
                dto.Id = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_cliente");
                dto.Carro = reader.GetString("ds_modelo");
                dto.Telefone = reader.GetString("ds_telefone");
                dto.Observacao = reader.GetString("ds_modelo");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<ClientesDTO> Listar()
        {
            string script = @"SELECT * FROM tb_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClientesDTO> lista = new List<ClientesDTO>();
            while (reader.Read())
            {
                ClientesDTO dto = new ClientesDTO();
                dto.Id = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_cliente");
                dto.Rg = reader.GetString("ds_rg");
                dto.CPF = reader.GetString("ds_cpf");
                dto.Nascimento = reader.GetDateTime("dt_nascimento");
                dto.Email = reader.GetString("ds_email");
                dto.Telefone = reader.GetString("ds_telefone");
                dto.Endereco = reader.GetString("ds_endereco");
                dto.Observacao = reader.GetString("ds_observacao");

                lista.Add(dto);
            }
            reader.Close();

            return lista;






        }
    }
}
