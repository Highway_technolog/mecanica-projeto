﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mecanica.DB.Cliente
{
    public class CarroBusiness
    {

        CarroDatabase db = new CarroDatabase();

        public int Salvar(CarroDTO Carro)
        {
            if (Carro.Modelo == string.Empty)
            {
                throw new ArgumentException("O modelo é obrigatória.");
            }
            if (Carro.Placa == string.Empty)
            {
                throw new ArgumentException("Placa é obrigatória.");
            }

            return db.Salvar(Carro);
        }

        public void Alterar(CarroDTO Carro)
        {
            if (Carro.Modelo == string.Empty)
            {
                throw new ArgumentException("Modelo é obrigatória.");
            }
            if (Carro.Placa == null)
            {
                throw new ArgumentException("Placa é obrigatória.");
            }

            db.Alterar(Carro);
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }

        public List<CarroDTO> Consultar(string modelo, string placa)
        {

            return db.Consultar(modelo, placa);
        }

        public List<CarroDTO> Listar()
        {
            CarroDatabase db = new CarroDatabase();
            return db.Listar();
        }

    }
}
