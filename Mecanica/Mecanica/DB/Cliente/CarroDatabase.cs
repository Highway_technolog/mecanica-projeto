﻿using Mecanica.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mecanica.DB.Cliente
{
    public class CarroDatabase
    {
        public int Salvar(CarroDTO cliente)
        {
            string script =
            @"INSERT INTO tb_carro
            (
	            ds_modelo,
	            ds_placa,
	            ds_observacao,
	            id_cliente,
	            ds_marca
            )
            VALUES
            (
	            @ds_modelo,
	            @ds_placa,
	            @ds_observacao,
	            @id_cliente,
	            @ds_marca
            )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_modelo", cliente.Modelo));
            parms.Add(new MySqlParameter("ds_placa", cliente.Placa));
            parms.Add(new MySqlParameter("ds_observacao", cliente.Observacao));
            parms.Add(new MySqlParameter("id_cliente", cliente.Id_Cliente));
            parms.Add(new MySqlParameter("ds_marca", cliente.Marca));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(CarroDTO carro)
        {
            string script =
            @"UPDATE tb_carro 
                 SET ds_marca       = @ds_marca,
	                 ds_placa       = @ds_placa,
	                 ds_observacao  = @ds_observacao,
	                 id_cliente     = @id_cliente,
	                 ds_marca       = @ds_marca
               WHERE id_carro       = @id_carro";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_carro", carro.Id));
            parms.Add(new MySqlParameter("ds_modelo", carro.Modelo));
            parms.Add(new MySqlParameter("ds_placa", carro.Placa));
            parms.Add(new MySqlParameter("ds_observacao", carro.Observacao));
            parms.Add(new MySqlParameter("id_cliente", carro.Id_Cliente));
            parms.Add(new MySqlParameter("ds_marca", carro.Marca));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(int id)
        {
            string script =
            @"DELETE FROM tb_carro WHERE id_carro = @id_carro";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_carro", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<CarroDTO> Consultar(string modelo, string placa)
        {
            string script =
            @"SELECT * 
                FROM tb_Carro
               WHERE ds_modelo like @ds_modelo
                 AND ds_placa like @ds_placa";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_modelo", "%" + modelo + "%"));
            parms.Add(new MySqlParameter("ds_placa", "%" + placa + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CarroDTO> Carro = new List<CarroDTO>();

            while (reader.Read())
            {
                CarroDTO carro = new CarroDTO();
                carro.Id = reader.GetInt32("id_carro");
                carro.Modelo = reader.GetString("ds_modelo");
                carro.Placa = reader.GetString("ds_placa");
                carro.Observacao = reader.GetString("ds_observacao");
                carro.Id_Cliente = reader.GetInt32("id_cliente");
                carro.Marca = reader.GetString("ds_marca");


                Carro.Add(carro);
            }
            reader.Close();

            return Carro;
        }


        public List<CarroDTO> Listar()
        {
            string script = @"SELECT * FROM tb_carro";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CarroDTO> lista = new List<CarroDTO>();
            while (reader.Read())
            {
                CarroDTO dto = new CarroDTO();
                dto.Id = reader.GetInt32("id_produto");
                dto.Modelo = reader.GetString("ds_modelo");
                dto.Placa = reader.GetString("ds_placa");
                dto.Id_Cliente = reader.GetInt32("id_cliente");
                dto.Marca = reader.GetString("ds_marca");

                lista.Add(dto);
            }
            reader.Close();

            return lista;







        }
    }
}
