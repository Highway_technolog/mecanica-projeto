﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mecanica.DB.Cliente
{
    public class MarcasBusiness
    {
        MarcasDatabase db = new MarcasDatabase();

        public int Salvar(MarcasDTO Marca)
        {
            if (Marca.Marca == string.Empty)
            {
                throw new ArgumentException("Marca é obrigatória.");
            }


            return db.Salvar(Marca);
        }

        public void Alterar(MarcasDTO Marca)
        {
            if (Marca.Marca == string.Empty)
            {
                throw new ArgumentException("música é obrigatória.");
            }

            db.Alterar(Marca);
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }

        public List<MarcasDTO> Consultar(string Marca)
        {

            return db.Consultar(Marca);
        }

        public List<MarcasDTO> Listar()
        {
            MarcasDatabase db = new MarcasDatabase();
            return db.Listar();
        }

    }
}
