﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mecanica.DB.Serviço
{
    public class PecasBusiness
    {
        PecasDatabase db = new PecasDatabase();

        public int Salvar(PecasDTO peca)
        {
            if (peca.Peca == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatória.");
            }
           

            return db.Salvar(peca);
        }

        public void Alterar(PecasDTO musica)
        {
            if (musica.Peca == string.Empty)
            {
                throw new ArgumentException("música é obrigatória.");
            }
            
            db.Alterar(musica);
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }

        public List<PecasDTO> Consultar(string Peca)
        {

            return db.Consultar(Peca);
        }



    }
}
