﻿using Mecanica.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mecanica.DB.Serviço
{
    public class PecasDatabase
    {
        public int Salvar(PecasDTO pecas)
        {
            string script =
            @"INSERT INTO tb_pecas
            (
	            nm_peca,
	            ds_preco
            )
            VALUES
            (
	            @nm_peca,
	            @ds_preco
	      
            )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_peca", pecas.Peca));
            parms.Add(new MySqlParameter("ds_preco", pecas.Preco));
           
            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(PecasDTO pecas)
        {
            string script =
            @"UPDATE tb_pecas
                 SET nm_peca  = @nm_peca,
	                 ds_preco       = @ds_preco
               WHERE id_pecas = @id_pecas";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", pecas.Peca));
            parms.Add(new MySqlParameter("nm_nome", pecas.Preco));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(int id)
        {
            string script =
            @"DELETE FROM tb_pecas WHERE id_pecas = @id_pecas";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pecas", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<PecasDTO> Consultar(string peca)
        {
            string script =
            @"SELECT * 
                FROM tb_pecas
               WHERE nm_peca like @nm_peca ";
                 

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", "%" + peca + "%"));
            

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PecasDTO> pecas = new List<PecasDTO>();

            while (reader.Read())
            {
                PecasDTO Peca = new PecasDTO();
                Peca.Id = reader.GetInt32("id_pecas");
                Peca.Peca = reader.GetString("nm_peca");
                

                pecas.Add(Peca);
            }
            reader.Close();

            return pecas;
        }








    }
}
