﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mecanica.DB.Serviço
{
    public class OrdemServicoDTO
    {
        public int Id { get; set; }
        public int Id_funcionario { get; set; }
        
        public int Id_cliente { get; set; }
        public decimal Valor { get; set; }
        public DateTime Dtservico { get; set; }
        public string descricao { get; set; }

    }
}
