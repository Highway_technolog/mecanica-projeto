﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mecanica.DB.Serviço
{
    public class OrdemServicoBusiness
    {
        OrdemServicoDatabase db = new OrdemServicoDatabase();

        public int Salvar(OrdemServicoDTO servico)
        {
            if (servico.Dtservico == null)
            {
                throw new ArgumentException("Nome é obrigatória.");
            }
            if (servico.Id_cliente == 0)
            {
                throw new ArgumentException("CPf é obrigatória.");
            }
            if (servico.Id_funcionario == 0)
            {
                throw new ArgumentException("Nascimento é obrigatória.");
            }

            return db.Salvar(servico);
        }

        public void Alterar(OrdemServicoDTO servico)
        {
            if (servico.Dtservico == null)
            {
                throw new ArgumentException("Nome é obrigatória.");
            }
            if (servico.Id_cliente == 0)
            {
                throw new ArgumentException("CPf é obrigatória.");
            }
            if (servico.Id_funcionario == 0)
            {
                throw new ArgumentException("Nascimento é obrigatória.");
            }


            db.Alterar(servico);
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }

        public List<OrdemServicoDTO> Consultar(string Nome, string nascimento)
        {

            return db.Consultar(Nome, nascimento);
        }

    }
}
