﻿using Mecanica.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mecanica.DB.Serviço
{
    public class OrdemServicoDatabase
    {
        public int Salvar(OrdemServicoDTO servico)
        {
            string script =
            @"INSERT INTO tb_ordem_servico
            (
	            ds_data,
	            ds_descricao,
	            ds_valor,
	            id_funcionario,
	            
                id_cliente
            )
            VALUES
            (
	            @ds_data,
	            @ds_descricao,
	            @ds_valor,
	            @id_funcionario,
	            
                @id_cliente
            )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_data", servico.Dtservico));
            parms.Add(new MySqlParameter("ds_descricao", servico.descricao));
            parms.Add(new MySqlParameter("ds_valor", servico.Valor));
            parms.Add(new MySqlParameter("id_funcionario", servico.Id_funcionario));
            
            parms.Add(new MySqlParameter("id_cliente", servico.Id_cliente));


            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(OrdemServicoDTO servico)
        {
            string script =
            @"UPDATE tb_ordem_servico 
                 SET ds_data                = @ds_data,
	                 ds_descricao           = @ds_descricao,
	                 ds_valor               = @ds_valor,
	                 id_funcionario         = @id_funcionario,
	                               
                     id_cliente             = @id_cliente
               WHERE id_ordem_servico       = @id_ordem_servico";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_ordem_servico", servico.Id));
            parms.Add(new MySqlParameter("ds_data", servico.Dtservico));
            parms.Add(new MySqlParameter("ds_descricao", servico.descricao));
            parms.Add(new MySqlParameter("ds_valor", servico.Valor));
            parms.Add(new MySqlParameter("id_funcionario", servico.Id_funcionario));
            
            parms.Add(new MySqlParameter("id_cliente", servico.Id_cliente));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(int id)
        {
            string script =
            @"DELETE FROM tb_ordem_servico WHERE id_ordem_servico = @id_ordem_servico";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_ordem_servico", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<OrdemServicoDTO> Consultar(string data, string cliente)
        {
            string script =
            @"SELECT * 
                FROM tb_ordem_servico
               WHERE ds_data like @ds_data
                 AND id_cliente like @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_modelo", "%" + data + "%"));
            parms.Add(new MySqlParameter("ds_placa", "%" + cliente + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<OrdemServicoDTO> Servico = new List<OrdemServicoDTO>();

            while (reader.Read())
            {
                OrdemServicoDTO servico = new OrdemServicoDTO();
                servico.Id = reader.GetInt32("id_ordem_servico");
                servico.Dtservico = reader.GetDateTime("ds_data");
                servico.descricao = reader.GetString("ds_descricao");
                servico.Valor = reader.GetDecimal("ds_valor");
                servico.Id_funcionario = reader.GetInt32("id_funcionario");
                
                servico.Id_cliente = reader.GetInt32("id_cliente");


                Servico.Add(servico);
            }
            reader.Close();

            return Servico;
        }



    }
}
