﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mecanica.DB.Serviço
{
    public class PecasDTO
    {
        public int Id { get; set; }
        public string Peca { get; set; }
        public double Preco { get; set; }

    }
}
