﻿using Mecanica.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mecanica.DB.Funcionario
{
    public class FuncionarioDatabase
    {
        public int Salvar(FuncionarioDTO funcionario)
        {
            string script =
            @"INSERT INTO tb_funcionarios
            (
	            nm_funcionario,
	            ds_rg,
	            ds_cpf,
	            dt_nascimento,
	            ds_email,
	            ds_salario,
	            ds_funcao,
                ds_endereco,
                ds_telefone,
                ds_celular,
                ds_senha
            )
            VALUES
            (
	            @nm_funcionario,
	            @ds_rg,
	            @ds_cpf,
                @dt_nascimento,
	            @ds_email,
	            @ds_salario,
	            @ds_funcao,     
                @ds_endereco,
                @ds_telefone,
                @ds_celular,
                @ds_senha
            )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", funcionario.Nome));
            parms.Add(new MySqlParameter("ds_rg", funcionario.RG));
            parms.Add(new MySqlParameter("ds_cpf", funcionario.CPF));
            parms.Add(new MySqlParameter("dt_nascimento", funcionario.Nascimento));
            parms.Add(new MySqlParameter("ds_email", funcionario.Email));
            parms.Add(new MySqlParameter("ds_salario", funcionario.Salario));
            parms.Add(new MySqlParameter("ds_funcao", funcionario.Funcao));
            parms.Add(new MySqlParameter("ds_endereco", funcionario.Endereco));
            parms.Add(new MySqlParameter("ds_telefone", funcionario.Telefone));
            parms.Add(new MySqlParameter("ds_celular", funcionario.Celular));
            parms.Add(new MySqlParameter("ds_senha", funcionario.Senha));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public void Alterar(FuncionarioDTO funcionario)
        {
            string script =
            @"UPDATE tb_funcionarios 
                 SET nm_funcionario  = @nm_funcionario,
	                 ds_rg           = @ds_rg,
	                 ds_cpf          = @ds_cpf,
	                 dt_nascimento   = @dt_nascimento,
	                 ds_email        = @ds_email,
	                 ds_salario      = @ds_salario,
	                 ds_funcao       = @ds_funcao,
                     ds_endereco     = @ds_endereco,
                     ds_telefone     = @ds_telefone,
                     ds_celular      = @ds_celular
                     ds_senha        = @ds_senha
               WHERE id_funcionarios       = @id_funcionarios";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionarios", funcionario.Id));
            parms.Add(new MySqlParameter("nm_funcionario", funcionario.Nome));
            parms.Add(new MySqlParameter("ds_rg", funcionario.RG));
            parms.Add(new MySqlParameter("ds_cpf", funcionario.CPF));
            parms.Add(new MySqlParameter("dt_nascimento", funcionario.Nascimento));
            parms.Add(new MySqlParameter("ds_email", funcionario.Email));
            parms.Add(new MySqlParameter("ds_salario", funcionario.Salario));
            parms.Add(new MySqlParameter("ds_funcao", funcionario.Funcao));
            parms.Add(new MySqlParameter("ds_endereco", funcionario.Endereco));
            parms.Add(new MySqlParameter("ds_telefone", funcionario.Telefone));
            parms.Add(new MySqlParameter("ds_celular", funcionario.Celular));
            parms.Add(new MySqlParameter("ds_senha", funcionario.Senha));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(int id)
        {
            string script =
            @"DELETE FROM tb_funcionarios WHERE id_funcionarios = @id_funcionarios";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionarios", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<FuncionarioDTO> Consultar(string nome)
        {
            string script =
            @"SELECT * 
                FROM tb_funcionarios
               WHERE nm_funcionario like @nm_funcionario";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", "%" + nome + "%"));


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> Funcionario = new List<FuncionarioDTO>();

            while (reader.Read())
            {
                FuncionarioDTO funcionario = new FuncionarioDTO();
                funcionario.Id = reader.GetInt32("id_funcionarios");
                funcionario.Nome = reader.GetString("nm_funcionario");
                funcionario.RG = reader.GetString("ds_rg");
                funcionario.CPF = reader.GetString("ds_cpf");
                funcionario.Nascimento = reader.GetDateTime("dt_nascimento");
                funcionario.Email = reader.GetString("ds_email");
                funcionario.Salario = reader.GetDouble("ds_salario");
                funcionario.Funcao = reader.GetString("ds_funcao");
                funcionario.Endereco = reader.GetString("ds_endereco");
                funcionario.Telefone = reader.GetString("ds_telefone");
                funcionario.Celular = reader.GetString("ds_celular");
                funcionario.Senha = reader.GetString("ds_senha");

                Funcionario.Add(funcionario);
            }
            reader.Close();

            return Funcionario;
        }


        public List<FuncionarioDTO> Listar()
        {
            string script = @"SELECT * FROM tb_funcionarios";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {

                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Id = reader.GetInt32("id_funcionarios");
                dto.Nome = reader.GetString("nm_funcionario");
                dto.RG = reader.GetString("ds_rg");
                dto.CPF = reader.GetString("ds_cpf");
                dto.Nascimento = reader.GetDateTime("dt_nascimento");
                dto.Email = reader.GetString("ds_email");
                dto.Salario = reader.GetDouble("ds_salario");
                dto.Funcao = reader.GetString("ds_funcao");
                dto.Endereco = reader.GetString("ds_endereco");
                dto.Telefone = reader.GetString("ds_telefone");
                dto.Celular = reader.GetString("ds_celular");
                dto.Senha = reader.GetString("ds_senha");


                lista.Add(dto);
            }
            reader.Close();

            return lista;



        }

        public bool Logar(string email, string senha)
        {
            string script = @"SELECT * FROM tb_funcionarios
                            WHERE ds_email    =  @ds_email
                            AND   ds_senha    =  @ds_senha";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_email", email));
            parms.Add(new MySqlParameter("ds_senha", senha));



            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            bool logou = false;

            if (reader.Read())
            {
                logou = true;
            }

            else
            {
                logou = false;


            }

            reader.Close();

            return logou;


        }
    }
}


   