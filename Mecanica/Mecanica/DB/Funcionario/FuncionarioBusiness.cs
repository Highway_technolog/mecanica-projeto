﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mecanica.DB.Funcionario
{
    public class FuncionarioBusiness
    {
        FuncionarioDatabase db = new FuncionarioDatabase();

        public int Salvar(FuncionarioDTO Funcionario)
        {
            if (Funcionario.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }
            if (Funcionario.CPF == string.Empty)
            {
                throw new ArgumentException("CPf é obrigatório.");
            }
            if (Funcionario.Nascimento == null)
            {
                throw new ArgumentException("Nascimento é obrigatório.");
            }

            if(Funcionario.Funcao == string.Empty)
            {
                throw new ArgumentException("Função é obrigatória");

            }

            return db.Salvar(Funcionario);
        }

        public void Alterar(FuncionarioDTO Funcionario)
        {
            if (Funcionario.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }
            if (Funcionario.CPF == string.Empty)
            {
                throw new ArgumentException("Função é obrigatório.");
            }
            if (Funcionario.Nascimento == null)
            {
                throw new ArgumentException("Nascimento é obrigatório.");
            }

            db.Alterar(Funcionario);
        }

        public void Remover(int id)
        {
            db.Remover(id);
        }

        public List<FuncionarioDTO> Consultar(string Nome)
        {

            return db.Consultar(Nome);
        }

        public List<FuncionarioDTO> Listar()
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Listar();
        }

        public bool Logar(string Email, string senha)
        {
            if(Email == string.Empty)
            {
                throw new ArgumentException("Usúario é obrigatória");

            }

            if (senha == string.Empty)
            {
                throw new ArgumentException("Senha é obrigatória");

            }

            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Logar(Email, senha);


        }

    }
}
