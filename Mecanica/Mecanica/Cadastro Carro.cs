﻿using Mecanica.DB;
using Mecanica.DB.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mecanica
{
    public partial class Cadastro_Carro : Form
    {
        public Cadastro_Carro()
        {
            InitializeComponent();
            CarregarCombos();
            CarregarMarcas();
        }

        private void label8_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();

            Menu cadastro = new Menu();

            cadastro.Show();
        }

        private void CarregarMarcas()
        {
            List<string> marca = new List<string>();
            marca.Add("Selecione");
            marca.Add("Chevrolet");
            marca.Add("Honda");
            marca.Add("Fiat");
            marca.Add("Mercedes");
            marca.Add("Toyota");
       

            cboMarca.DataSource = marca;
        }



        void CarregarCombos()
        {
            

            ClienteBusiness business = new ClienteBusiness();
            List<ClientesDTO> listar = business.Listar();

            cboCliente.ValueMember = nameof(ClientesDTO.Id);
            cboCliente.DisplayMember = nameof(ClientesDTO.Nome);
            cboCliente.DataSource = listar;
        }

     
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                ClientesDTO categoria = cboCliente.SelectedItem as ClientesDTO;



                CarroDTO dto = new CarroDTO();
                dto.Modelo = maskedtxtModelo.Text;
                dto.Placa = maskedtxtPlaca.Text;
                dto.Observacao = txtObservacao.Text;
                dto.Id_Cliente = categoria.Id;
                dto.Marca = cboMarca.SelectedItem.ToString();

                CarroBusiness business = new CarroBusiness();
                business.Salvar(dto);

                EnviarMensagem("Carro salvo com sucesso.");

            }
            catch (Exception)
            {

                MessageBox.Show("Use informações válidas ");
            }
               


          


        }

        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "Nsf Jam",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }


        private void EnviarMensagem(string mensagem)
        {
            MessageBox.Show(mensagem, "MARBRU",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Information);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void Voltar_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Hide();

            Menu_Principal cadastro = new Menu_Principal();

            cadastro.Show();
        }

        private void maskedTextBox3_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void maskedtxtModelo_KeyPress(object sender, KeyPressEventArgs e)
        {
           

        }
    }
}
