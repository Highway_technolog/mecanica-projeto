﻿using Mecanica.DB.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mecanica
{
    public partial class Menu_Principal : Form
    {
        public Menu_Principal()
        {
            InitializeComponent();
        }

        private void Menu_Principal_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
           
            Menu cadastro = new Menu();
           
            cadastro.Show();


        }

        private void button2_Click(object sender, EventArgs e)
        {

            this.Hide();

            Menu_Consulta cadastro = new Menu_Consulta();

            cadastro.Show();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();

            Tela_serviço cadastro = new Tela_serviço();

            cadastro.Show();
        }
    }
}
